#include "idt.h"

#include "io/io.h"

struct Idt_Entry idt_entries[IDT_SIZE];

struct Idt_Ptr idt_ptr;

#define SET_HANDLER(number)                                                    \
    extern void handler_##number(void);                                        \
    set_interrupt(number, handler_##number);

void init_idt(void)
{
    SET_HANDLER(0)
    SET_HANDLER(1)
    SET_HANDLER(2)
    SET_HANDLER(3)
    SET_HANDLER(4)
    SET_HANDLER(5)
    SET_HANDLER(6)
    SET_HANDLER(7)
    SET_HANDLER(8)
    SET_HANDLER(9)
    SET_HANDLER(10)
    SET_HANDLER(11)
    SET_HANDLER(12)
    SET_HANDLER(13)
    SET_HANDLER(14)
    SET_HANDLER(15)
    SET_HANDLER(16)
    SET_HANDLER(17)
    SET_HANDLER(18)
    SET_HANDLER(19)
    SET_HANDLER(20)
    SET_HANDLER(21)
    SET_HANDLER(22)
    SET_HANDLER(23)
    SET_HANDLER(24)
    SET_HANDLER(25)
    SET_HANDLER(26)
    SET_HANDLER(27)
    SET_HANDLER(28)
    SET_HANDLER(29)
    SET_HANDLER(30)
    SET_HANDLER(31)
    SET_HANDLER(32)
    SET_HANDLER(33)
    SET_HANDLER(34)
    SET_HANDLER(35)
    SET_HANDLER(36)
    SET_HANDLER(37)
    SET_HANDLER(38)
    SET_HANDLER(39)
    SET_HANDLER(40)
    SET_HANDLER(41)
    SET_HANDLER(42)
    SET_HANDLER(43)
    SET_HANDLER(44)
    SET_HANDLER(45)
    SET_HANDLER(46)
    SET_HANDLER(47)
    SET_HANDLER(48)
    SET_HANDLER(49)
    SET_HANDLER(50)
    SET_HANDLER(51)
    SET_HANDLER(52)
    SET_HANDLER(53)
    SET_HANDLER(54)
    SET_HANDLER(55)
    SET_HANDLER(56)
    SET_HANDLER(57)
    SET_HANDLER(58)
    SET_HANDLER(59)
    SET_HANDLER(60)
    SET_HANDLER(61)
    SET_HANDLER(62)
    SET_HANDLER(63)
    SET_HANDLER(64)
    SET_HANDLER(65)
    SET_HANDLER(66)
    SET_HANDLER(67)
    SET_HANDLER(68)
    SET_HANDLER(69)
    SET_HANDLER(70)
    SET_HANDLER(71)
    SET_HANDLER(72)
    SET_HANDLER(73)
    SET_HANDLER(74)
    SET_HANDLER(75)
    SET_HANDLER(76)
    SET_HANDLER(77)
    SET_HANDLER(78)
    SET_HANDLER(79)
    SET_HANDLER(80)
    SET_HANDLER(81)
    SET_HANDLER(82)
    SET_HANDLER(83)
    SET_HANDLER(84)
    SET_HANDLER(85)
    SET_HANDLER(86)
    SET_HANDLER(87)
    SET_HANDLER(88)
    SET_HANDLER(89)
    SET_HANDLER(90)
    SET_HANDLER(91)
    SET_HANDLER(92)
    SET_HANDLER(93)
    SET_HANDLER(94)
    SET_HANDLER(95)
    SET_HANDLER(96)
    SET_HANDLER(97)
    SET_HANDLER(98)
    SET_HANDLER(99)
    SET_HANDLER(100)
    SET_HANDLER(101)
    SET_HANDLER(102)
    SET_HANDLER(103)
    SET_HANDLER(104)
    SET_HANDLER(105)
    SET_HANDLER(106)
    SET_HANDLER(107)
    SET_HANDLER(108)
    SET_HANDLER(109)
    SET_HANDLER(110)
    SET_HANDLER(111)
    SET_HANDLER(112)
    SET_HANDLER(113)
    SET_HANDLER(114)
    SET_HANDLER(115)
    SET_HANDLER(116)
    SET_HANDLER(117)
    SET_HANDLER(118)
    SET_HANDLER(119)
    SET_HANDLER(120)
    SET_HANDLER(121)
    SET_HANDLER(122)
    SET_HANDLER(123)
    SET_HANDLER(124)
    SET_HANDLER(125)
    SET_HANDLER(126)
    SET_HANDLER(127)
    SET_HANDLER(128)
    SET_HANDLER(129)
    SET_HANDLER(130)
    SET_HANDLER(131)
    SET_HANDLER(132)
    SET_HANDLER(133)
    SET_HANDLER(134)
    SET_HANDLER(135)
    SET_HANDLER(136)
    SET_HANDLER(137)
    SET_HANDLER(138)
    SET_HANDLER(139)
    SET_HANDLER(140)
    SET_HANDLER(141)
    SET_HANDLER(142)
    SET_HANDLER(143)
    SET_HANDLER(144)
    SET_HANDLER(145)
    SET_HANDLER(146)
    SET_HANDLER(147)
    SET_HANDLER(148)
    SET_HANDLER(149)
    SET_HANDLER(150)
    SET_HANDLER(151)
    SET_HANDLER(152)
    SET_HANDLER(153)
    SET_HANDLER(154)
    SET_HANDLER(155)
    SET_HANDLER(156)
    SET_HANDLER(157)
    SET_HANDLER(158)
    SET_HANDLER(159)
    SET_HANDLER(160)
    SET_HANDLER(161)
    SET_HANDLER(162)
    SET_HANDLER(163)
    SET_HANDLER(164)
    SET_HANDLER(165)
    SET_HANDLER(166)
    SET_HANDLER(167)
    SET_HANDLER(168)
    SET_HANDLER(169)
    SET_HANDLER(170)
    SET_HANDLER(171)
    SET_HANDLER(172)
    SET_HANDLER(173)
    SET_HANDLER(174)
    SET_HANDLER(175)
    SET_HANDLER(176)
    SET_HANDLER(177)
    SET_HANDLER(178)
    SET_HANDLER(179)
    SET_HANDLER(180)
    SET_HANDLER(181)
    SET_HANDLER(182)
    SET_HANDLER(183)
    SET_HANDLER(184)
    SET_HANDLER(185)
    SET_HANDLER(186)
    SET_HANDLER(187)
    SET_HANDLER(188)
    SET_HANDLER(189)
    SET_HANDLER(190)
    SET_HANDLER(191)
    SET_HANDLER(192)
    SET_HANDLER(193)
    SET_HANDLER(194)
    SET_HANDLER(195)
    SET_HANDLER(196)
    SET_HANDLER(197)
    SET_HANDLER(198)
    SET_HANDLER(199)
    SET_HANDLER(200)
    SET_HANDLER(201)
    SET_HANDLER(202)
    SET_HANDLER(203)
    SET_HANDLER(204)
    SET_HANDLER(205)
    SET_HANDLER(206)
    SET_HANDLER(207)
    SET_HANDLER(208)
    SET_HANDLER(209)
    SET_HANDLER(210)
    SET_HANDLER(211)
    SET_HANDLER(212)
    SET_HANDLER(213)
    SET_HANDLER(214)
    SET_HANDLER(215)
    SET_HANDLER(216)
    SET_HANDLER(217)
    SET_HANDLER(218)
    SET_HANDLER(219)
    SET_HANDLER(220)
    SET_HANDLER(221)
    SET_HANDLER(222)
    SET_HANDLER(223)
    SET_HANDLER(224)
    SET_HANDLER(225)
    SET_HANDLER(226)
    SET_HANDLER(227)
    SET_HANDLER(228)
    SET_HANDLER(229)
    SET_HANDLER(230)
    SET_HANDLER(231)
    SET_HANDLER(232)
    SET_HANDLER(233)
    SET_HANDLER(234)
    SET_HANDLER(235)
    SET_HANDLER(236)
    SET_HANDLER(237)
    SET_HANDLER(238)
    SET_HANDLER(239)
    SET_HANDLER(240)
    SET_HANDLER(241)
    SET_HANDLER(242)
    SET_HANDLER(243)
    SET_HANDLER(244)
    SET_HANDLER(245)
    SET_HANDLER(246)
    SET_HANDLER(247)
    SET_HANDLER(248)
    SET_HANDLER(249)
    SET_HANDLER(250)
    SET_HANDLER(251)
    SET_HANDLER(252)
    SET_HANDLER(253)
    SET_HANDLER(254)
}

void init_pic(void)
{
    // Send init command to both
    outb(PIC1, 0x11); // ICW4 present + cascade mode + edge triggered
    outb(PIC2, 0x11);

    // ICW2
    outb(PIC1_DATA, OFFSET_PIC1); // Set the offset to 32 on PIC1
    outb(PIC2_DATA, OFFSET_PIC2); // Set the offset to 64 on PIC2

    // ICW3
    outb(PIC1_DATA, 0x4); // 00000100
    outb(PIC2_DATA, 0x2); // 00000010

    // ICW4
    outb(PIC1_DATA, 0x1);
    outb(PIC2_DATA, 0x1);

    // mask all interupts
    outb(PIC1_DATA, 0xFF);
    outb(PIC2_DATA, 0xFF);
}

void set_interrupt(u32 index, void (*handler)(void))
{
    idt_entries[index].offset_lower_bits = ((unsigned long)handler) & 0xFFFF;
    idt_entries[index].offset_higher_bits =
        (((unsigned long)handler) & 0xFFFF0000) >> 16;
    idt_entries[index].type_attr = IDT_32_ATTR;
    idt_entries[index].selector = 0x10;
    idt_entries[index].zero = 0;
}

void load_idt(void)
{
    idt_ptr.ptr = idt_entries;
    idt_ptr.limit = sizeof(struct Idt_Entry) * IDT_SIZE;

    __asm__("lidt %0" ::"m"(idt_ptr));
}

void enable_interupts()
{
    __asm__ volatile("sti");
}

void disable_interupt()
{
    __asm__ volatile("cli");
}

void enable_irqA(u8 irq)
{
    u8 current = inb(PIC1_DATA);
    current &= ~(1 << irq);

    outb(PIC1_DATA, current);
}

void disavle_irqA(u8 irq)
{
    u8 current = inb(PIC1_DATA);
    current |= (1 << irq);

    outb(PIC1_DATA, current);
}

void send_eoi()
{
    outb(PIC1, EOI);
    outb(PIC2, EOI);
}