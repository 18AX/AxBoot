#ifndef IDT_HEADER
#define IDT_HEADER

#include "k/types.h"

#define IDT_SIZE 0xFF
#define IDT_32_ATTR 0x8E

#define PIC1 0x20
#define PIC2 0xA0
#define PIC1_DATA 0x21
#define PIC2_DATA 0xA1

#define OFFSET_PIC1 0x40
#define OFFSET_PIC2 0x50

#define EOI 0x20

struct Idt_Entry
{
    u16 offset_lower_bits;
    u16 selector;
    u8 zero;
    u8 type_attr;
    u16 offset_higher_bits;
} __attribute__((packed));

struct Idt_Ptr
{
    u16 limit;
    struct Idt_Entry *ptr;
} __attribute__((packed));

void init_idt(void);

void init_pic(void);

void set_interrupt(u32 index, void (*handler)(void));

void load_idt(void);

void enable_interupts();

void disable_interupt();

void enable_irqA(u8 irq);

void disavle_irqA(u8 irq);

void send_eoi();

#endif