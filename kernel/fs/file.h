#ifndef FILE_HEADER
#define FILE_HEADER

#define SEEK_SET 0
#define SEEK_END 2
#define SEEK_CUR 1

#include "k/types.h"

int open(char *pathname);

long int read(int fd, void *buf, u32 count);

long seek(int fd, long offset, int whence);

int close(int fd);

#endif