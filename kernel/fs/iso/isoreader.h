#ifndef ISO_READER_HEADER
#define ISO_READER_HEADER

#include "k/types.h"

struct file
{
    u32 sector;
    u32 size;
    u32 cursor;
    int open;
    char path[255];
};

int iso_find_file(char *path, struct file *file);

#endif