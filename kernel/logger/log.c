#if DEBUG

#include "k/log.h"
#include "utils/string.h"
#include "k/stdio.h"
#include "serial/write.h"

int printf(const char *fmt, ...)
{

    char printf_buf[1024];
    va_list args;
    int printed;

    va_start(args, fmt);
    printed = vsprintf(printf_buf, fmt, args);
    va_end(args);

    puts(printf_buf);

    return printed;
}

int puts(const char *s)
{
    return write(s, strlen(s));
}


void log_info(const char *txt, ...)
{
    char printf_buf[1024];
    va_list args;
    va_start(args, txt);
    printf("[\033[0;34m*\033[0m] ");
    vsprintf(printf_buf, txt, args);
    printf(printf_buf);
    printf("\n");
    va_end(args);

}

void log_success(const char *txt, ...)
{
    char printf_buf[1024];
    va_list args;
    va_start(args, txt);
    printf("[\033[0;32m+\033[0m] ");
    vsprintf(printf_buf, txt, args);
    printf(printf_buf);
    printf("\n");
    va_end(args);

}

void log_error(const char *txt, ...)
{
    char printf_buf[1024];
    va_list args;
    va_start(args, txt);
    printf("[\033[0;31m-\033[0m] ");
    vsprintf(printf_buf, txt, args);
    printf(printf_buf);
    printf("\n");
    va_end(args);

}
#endif