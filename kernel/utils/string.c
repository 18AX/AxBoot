#include "string.h"

unsigned long strlen(const char *str)
{
    unsigned long i = 0;
    for (; str[i] != '\0'; ++i)
        ;

    return i;
}

u32 strnlen(const char *s, u32 maxlen)
{
    u32 i = 0;
    for (; i < maxlen; ++i)
        if (!s[i])
            return i;
    return i;
}

void *memcpy(void *dest, const void *src, u32 n)
{
    const char *s = src;
    char *d = dest;

    for (u32 i = 0; i < n; i++)
        *d++ = *s++;

    return dest;
}

int strcmp(const char *s1, const char *s2)
{
    for (; *s1 == *s2 && *s1 != '\0'; s1++, s2++)
        continue;

    return *s1 - *s2;
}

void *memset(void *s, int c, u32 n)
{
    char *p = s;

    for (u32 i = 0; i < n; ++i)
        p[i] = c;

    return s;
}

char *strcpy(char *dest, const char *src)
{
    char *p = dest;

    while (*src)
        *p++ = *src++;

    *p = '\0';

    return dest;
}

void strupper(char *str)
{
    for (; *str != '\0'; ++str)
    {
        if (*str >= 'a' && *str <= 'z')
        {
            *str = 'A' + *str - 'a';
        }
    }
}