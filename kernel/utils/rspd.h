#ifndef RSPD_HEADER
#define RSPD_HEADER

#define EBDA 0x040E << 4

void *get_rspd_ptr();

#endif