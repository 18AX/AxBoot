#include "utils/rspd.h"

#include "k/types.h"
#include "utils/string.h"

void *get_rspd_ptr()
{
    char *ebda = (char*) (EBDA);

    for (u32 i = 0; ebda + i < (char*) 0x100000; i += 16)
    {
        if (strcmp(&ebda[i], "RSD PTR "))
        {
            return &ebda[i];
        }
    }

    return NULL;
}