#include "ui.h"

#include "draw/draw.h"
#include "draw/print.h"
#include "driver/keyboard/keyboard.h"
#include "k/kstd.h"
#include "k/log.h"
#include "k/stdio.h"

void waiting_screen()
{
    print("Press any key to continue", 10, 10,
          (struct rgb){ 0xFF, 0xFF, 0xFF });

    while (getkey() == -1)
        ;
}

void die_with_error(int code)
{
    LOG_ERROR("die with error %d", code);

    clear_screen((struct rgb){ 0, 0, 0 });
    char buffer[1024];

    sprintf(buffer, "AxBoot unexpected error 0x%.2x", code);
    print(buffer, 10, 10, (struct rgb){ 0xFF, 0, 0 });

    update_screen();

    for (;;)
        __asm__ volatile("hlt");
}

void die_with_error_message(const char *messge)
{
    LOG_ERROR("die with error : %s", messge);

    clear_screen((struct rgb){ 0, 0, 0 });
    char buffer[1024];

    sprintf(buffer, "AxBoot: %s", messge);
    print(buffer, 10, 10, (struct rgb){ 0xFF, 0, 0 });

    update_screen();

    for (;;)
        __asm__ volatile("hlt");
}

void die()
{
    for (;;)
        __asm__ volatile("hlt");
}