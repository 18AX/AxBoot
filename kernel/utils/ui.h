#ifndef UI_HEADER
#define UI_HEADER

void waiting_screen();

void die_with_error(int code);

void die_with_error_message(const char *messge);

void die();

#endif