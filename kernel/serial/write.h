#ifndef WRITE_HEADER
#define WRITE_HEADER

#include "k/types.h"

int write(const char *buffer, u32 count);

#endif