#ifndef LINUX_HEADER
#define LINUX_HEADER

#define LINUX_REAL_MODE_LOAD_ADDRESS 0x90000
#define REAL_MODE_MAX_SIZE 32768
#define LINUX_LOAD_ADDRESS 0x100000

#define LINUX_LOAD_TMP_ADDRESS 0xF00000

#include "k/bootinfo.h"

int boot_linux(char *kernel_path, char *initrd_path, struct boot_info *info);

#endif