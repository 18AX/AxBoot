section .text

[BITS 32]
global linux_kernel_jmp

linux_kernel_jmp:
    cli

    mov esi,[esp+4]

    xor ebp,ebp
    xor edi,edi
    xor ebx,ebx

    jmp 0x100000
