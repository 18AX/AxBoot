#include "linux.h"

#include "fs/file.h"
#include "k/bootparams.h"
#include "k/e820.h"
#include "k/log.h"
#include "memory/allocator.h"
#include "utils/rspd.h"
#include "utils/string.h"

extern void linux_kernel_jmp(void *ptr);

static void linux_setup(struct boot_params *boot_params, struct boot_info *info)
{
    u32 base_address = LINUX_REAL_MODE_LOAD_ADDRESS;

    boot_params->hdr.type_of_loader = 0xFF;

    boot_params->hdr.ext_loader_ver = 0;
    boot_params->hdr.ext_loader_type = 0;

    char *heap_end = (char*) 0xe000;

    char *cmd_line_ptr = base_address + heap_end;

    char cmd[] = "auto";

    strcpy(cmd_line_ptr, cmd);

    boot_params->hdr.cmdline_size = strlen(cmd);
    boot_params->hdr.cmd_line_ptr = (u32) cmd_line_ptr;

    boot_params->hdr.heap_end_ptr = (u32) (heap_end - 0x200);
    boot_params->hdr.loadflags |= 0x80;
    boot_params->hdr.loadflags &= ~(1 << 5); // Dont set up quiet

    // e820

    boot_params->e820_entries = info->e820_size;

    for (u8 i = 0; i < info->e820_size; ++i)
    {
        u64 base =
            (((u64)info->e820_ptr[i].BaseH) << 32) | info->e820_ptr[i].BaseL;
        u64 len = (((u64)info->e820_ptr[i].LengthH) << 32) | info->e820_ptr[i].LengthL;

        boot_params->e820_table[i].addr = base;
        boot_params->e820_table[i].size = len;
        boot_params->e820_table[i].type = info->e820_ptr[i].Type;
    }

    // video

    boot_params->hdr.vid_mode = 0xFFFF;

    boot_params->screen_info.capabilities = info->vbe_info->capabilities;
    boot_params->screen_info.lfb_base = info->vbe_mode_info_ptr->framebuffer;
    boot_params->screen_info.ext_lfb_base = 0;

    boot_params->screen_info.lfb_size =
        info->vbe_mode_info_ptr->pitch * info->vbe_mode_info_ptr->height;
    boot_params->screen_info.lfb_width = info->vbe_mode_info_ptr->width;
    boot_params->screen_info.lfb_height = info->vbe_mode_info_ptr->height;
    boot_params->screen_info.lfb_depth = info->vbe_mode_info_ptr->bpp;

    if (info->vbe_info->version < 3)
    {
        boot_params->screen_info.lfb_linelength =
            info->vbe_mode_info_ptr->pitch;

        boot_params->screen_info.blue_pos =
            info->vbe_mode_info_ptr->blue_position;
        boot_params->screen_info.blue_size = info->vbe_mode_info_ptr->blue_mask;

        boot_params->screen_info.red_pos =
            info->vbe_mode_info_ptr->red_position;
        boot_params->screen_info.red_size = info->vbe_mode_info_ptr->red_mask;

        boot_params->screen_info.green_pos =
            info->vbe_mode_info_ptr->green_position;
        boot_params->screen_info.green_size =
            info->vbe_mode_info_ptr->green_mask;
    }
    else
    {
        boot_params->screen_info.lfb_linelength =
            info->vbe_mode_info_ptr->lin_bytes_per_scanline;

        boot_params->screen_info.blue_pos =
            info->vbe_mode_info_ptr->lin_blue_mask_shift;

        boot_params->screen_info.blue_size =
            info->vbe_mode_info_ptr->lin_blue_mask_size;

        boot_params->screen_info.red_pos =
            info->vbe_mode_info_ptr->lin_red_mask_shift;

        boot_params->screen_info.red_size =
            info->vbe_mode_info_ptr->lin_red_mask_size;

        boot_params->screen_info.green_pos =
            info->vbe_mode_info_ptr->lin_green_mask_shift;

        boot_params->screen_info.green_size =
            info->vbe_mode_info_ptr->lin_green_mask_size;
    }

    boot_params->screen_info.orig_video_isVGA = 0x23;

    boot_params->acpi_rsdp_addr = (u32) get_rspd_ptr();
}

static void load_kernel_memory(struct setup_header *hdr, void *real_mode_addr,
                               void *kernel_addr, u8 *buffer, u32 size)
{
    u32 setup_sects = hdr->setup_sects;

    if (setup_sects == 0)
    {
        setup_sects = 4;
    }

    u32 setup_size = (setup_sects + 1) * 512;

    u32 kernel_size = size - setup_sects;

    // Load Setup

    memcpy(real_mode_addr, buffer, setup_size);

    // Load Kernel

    memcpy(kernel_addr, buffer + setup_size, kernel_size);
}

static int load_initrd(char *path, struct boot_params *params)
{
    if (strlen(path) == 0)
    {
        return 0;
    }

    int initrd_fd = open(path);

    if (initrd_fd == -1)
    {
        return 0;
    }

    u32 initrdsize = seek(initrd_fd, 0, SEEK_END);
    seek(initrd_fd, 0, SEEK_SET);

    u8 *buffer = NULL;

    if (params->hdr.initrd_addr_max == 0)
    {
        buffer = allocate(initrdsize);
    }
    else
    {
        void* ptr = (void*) ((params->hdr.initrd_addr_max - initrdsize) & -4096);
        buffer = allocate_at(ptr, initrdsize);
    }

    if (buffer == ALLOCATOR_FAILED)
    {
        return 0;
    }

    if (read(initrd_fd, buffer, initrdsize) <= 0)
    {
        return 0;
    }

    params->hdr.ramdisk_image = (u32) buffer;
    params->hdr.ramdisk_size = initrdsize;

    return 1;
}

int boot_linux(char *kernel_path, char *initrd_path,
               struct boot_info *info)
{
    int kernel_fd = open(kernel_path);

    if (kernel_fd == -1)
    {
        LOG_ERROR("cannot find kernel image");
        return 0;
    }

    u32 filesize = seek(kernel_fd, 0, SEEK_END);

    seek(kernel_fd, 0, SEEK_SET);

    // Allocate real mode space
    u8 *real_mode_addr =
        allocate_at((void*) LINUX_REAL_MODE_LOAD_ADDRESS, REAL_MODE_MAX_SIZE);

    if (real_mode_addr == ALLOCATOR_FAILED)
    {
        LOG_ERROR("cannot allocate real mode");
        return -1;
    }

    memset(real_mode_addr, 0, REAL_MODE_MAX_SIZE);

    // Allocate kernel space
    u8 *kernel_addr = allocate_at((void*) LINUX_LOAD_ADDRESS, filesize);

    if (kernel_addr == ALLOCATOR_FAILED)
    {
        LOG_ERROR("cannot allocate kernel");
        return -1;
    }

    memset(kernel_addr, 0, filesize);

    // Allocate tmp buffer
    u8 *buffer = allocate(filesize);

    if (buffer == ALLOCATOR_FAILED)
    {
        LOG_ERROR("cannot allocate tmp buffer");
        return -1;
    }

    // read the kernel to the tmp buffer
    if (read(kernel_fd, buffer, filesize) <= 0)
    {
        LOG_ERROR("cannot read kernel");
        return 0;
    }

    close(kernel_fd);

    struct setup_header *setup = (struct setup_header*) (buffer + 0x1F1);

    // Not supported kernel
    if ((setup->loadflags & 0x1) == 0)
    {
        LOG_ERROR("not supported kernel");
        return -1;
    }

    // Copy real and 32 bits kernel at the right addressses
    load_kernel_memory(setup, real_mode_addr, kernel_addr, buffer, filesize);

    setup = (struct setup_header*) (real_mode_addr + 0x1F1);

    struct boot_params *boot_params = allocate(sizeof(struct boot_params));

    if (boot_params == ALLOCATOR_FAILED)
    {
        LOG_ERROR("cannot allocate boot params");
        return -1;
    }

    memset(boot_params, 0, sizeof(struct boot_params));

    memcpy(&boot_params->hdr, setup, sizeof(struct setup_header));

    // Setup boot params
    linux_setup(boot_params, info);

    // load initrd
    if (load_initrd(initrd_path, boot_params) != 1)
    {
        LOG_ERROR("cannot load initrd");
    }

    LOG_SUCCESS("Set up done boot params at %p %d", boot_params,
                boot_params->sentinel);

    LOG_SUCCESS("Launching Linux...");

    linux_kernel_jmp(boot_params);

    return 1;
}