#ifndef DRAW_HEADER
#define DRAW_HEADER

#include "k/vbe.h"

#define VGA 0xA0000

#define SCREEN_WIDTH_PIXEL 320
#define SCREEN_HEIGH_PIXEL 200

struct rgb
{
    u8 red;
    u8 green;
    u8 blue;
};

void draw_init(struct vbe_mode_info_structure *vbe_info);

void draw_char(unsigned char c, int x, int y, struct rgb rgb);

void draw_rectangle(int x, int y, int width, int heigh, struct rgb rgb);

void put_pixel(int x, int y, struct rgb rgb);

void clear_screen(struct rgb rgb);

void update_screen();

void draw_destroy();

#endif