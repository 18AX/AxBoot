#include "menu.h"

#include "driver/keyboard/keyboard.h"
#include "memory/allocator.h"
#include "print.h"
#include "utils/string.h"

struct menu *create_menu_from_config(struct vbe_mode_info_structure *vbe_info,
                                     struct config *config)
{
    struct menu *menu = allocate(sizeof(struct menu));
    if (menu == ALLOCATOR_FAILED)
    {
        return NULL;
    }

    menu->items = allocate(sizeof(struct menu_item) * config->size);

    if (menu->items == ALLOCATOR_FAILED)
    {
        return NULL;
    }

    struct config_elt *curr = config->elements;

    u32 i = 0;

    while (curr != NULL)
    {
        menu->items[i].is_focus = 0;
        strcpy(menu->items[i].value, curr->name);

        ++i;
        curr = curr->next;
    }

    menu->background = (struct rgb){ 0xA, 0xA, 0xA };
    menu->foreground = (struct rgb){ 0xFF, 0xFF, 0xFF };
    menu->focus_background = (struct rgb){ 0xF0, 0xF0, 0xF0 };
    menu->focus_foreground = (struct rgb){ 0x5, 0x5, 0x5 };

    menu->size = config->size;
    menu->width = vbe_info->width - 200;
    menu->height = vbe_info->height - 200;
    menu->x = 100;
    menu->y = 100;

    return menu;
}

static void update_menu(struct menu *menu)
{
    draw_rectangle(menu->x, menu->y, menu->width, menu->height,
                   menu->background);

    print("AxBoot", menu->x + MARGIN_LEFT, menu->y + MARGIN_UP,
          menu->foreground);

    u32 pos_x = menu->x + MARGIN_LEFT;

    u32 pos_y = menu->y + 3 * MARGIN_UP;

    for (u32 i = 0; i < menu->size; ++i)
    {
        if (menu->items[i].is_focus)
        {
            draw_rectangle(menu->x, pos_y - 10, menu->width, ITEM_HEIGH,
                           menu->focus_background);
            print(menu->items[i].value, pos_x, pos_y, menu->focus_foreground);
        }
        else
        {
            print(menu->items[i].value, pos_x, pos_y, menu->foreground);
        }

        pos_y += ITEM_HEIGH;
    }
}

u32 draw_menu(struct menu *menu)
{
    if (menu->size <= 0)
    {
        return -1;
    }

    menu->items[0].is_focus = 1;

    update_menu(menu);

    update_screen();

    u32 current = 0;

    for (;;)
    {
        int key = wait_key();

        menu->items[current].is_focus = 0;

        switch (key)
        {
        case K_ARROW_DOWN:
            current = (current + 1) % menu->size;
            break;
        case K_ARROW_UP:
            if (current == 0)
            {
                current = menu->size - 1;
            }
            else
            {
                --current;
            }
            break;
        case K_ENTER:
            return current;
        }

        menu->items[current].is_focus = 1;

        update_menu(menu);
        update_screen();
    }
}

/**
void draw_menu(struct menu *menu)
{
    int x_middle = SCREEN_WIDTH_PIXEL / 2;
    int y_middle = SCREEN_HEIGH_PIXEL / 2;

    int menu_pos_y = y_middle - (ITEM_HEIGH * (menu->size + 1) / 2);
    int menu_pos_x = x_middle - (MENU_WIDTH / 2);

    draw_rectangle(menu_pos_x, menu_pos_y, MENU_WIDTH,
                   ITEM_HEIGH * (menu->size + 1), menu->background);

    print(menu->title, menu_pos_x + 10, menu_pos_y + 7, menu->foreground);

    for (unsigned long i = 0; i < menu->size; ++i)
    {
        int item_pos_y = (i + 1) * ITEM_HEIGH + menu_pos_y;

        if (menu->items[i].is_focus == 1)
        {
            draw_rectangle(menu_pos_x, item_pos_y, MENU_WIDTH, ITEM_HEIGH,
                           menu->focus_background);
            print(menu->items[i].value, menu_pos_x + 10, item_pos_y + 7,
                  menu->focus_foreground);
        }
        else
        {
            print(menu->items[i].value, menu_pos_x + 10, item_pos_y + 7,
                  menu->foreground);
        }
    }
}
**/