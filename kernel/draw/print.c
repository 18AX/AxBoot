#include "print.h"

#include "utils/string.h"

void print(const char *str, int x, int y, struct rgb rgb)
{
    unsigned long len = strlen(str);

    for (unsigned long i = 0; i < len; ++i)
    {
        draw_char(str[i], x + i * 8, y, rgb);
    }
}
