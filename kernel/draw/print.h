#ifndef PRINT_HEADER
#define PRINT_HEADER

#include "draw.h"

void print(const char *str, int x, int y, struct rgb rgb);

#endif