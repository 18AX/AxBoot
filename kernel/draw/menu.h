#ifndef MENU_HEADER
#define MENU_HEADER

#include "config/config.h"
#include "draw.h"
#include "k/vbe.h"

#define MARGIN_LEFT 15
#define MARGIN_UP 15
#define ITEM_HEIGH 25

struct menu_item
{
    char value[0xFF];
    int is_focus;
};

struct menu
{
    char *title;
    u32 size;
    u32 x;
    u32 y;
    u32 width;
    u32 height;
    struct menu_item *items;
    struct rgb background;
    struct rgb foreground;
    struct rgb focus_background;
    struct rgb focus_foreground;
};

struct menu *create_menu_from_config(struct vbe_mode_info_structure *vbe_info,
                                     struct config *config);

u32 draw_menu(struct menu *menu);

#endif