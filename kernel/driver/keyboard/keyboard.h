#ifndef KEYBOARD_HEADER
#define KEYBOARD_HEADER

#include "k/types.h"

#define KEYBOARD_STATUS_PORT 0x64
#define KEYBOARD_READ_PORT 0x60

#define KEY_QUEUE_SIZE 128

#define K_ARROW_UP 72
#define K_ARROW_DOWN 80
#define K_ARROW_LEFT 75
#define K_ARROW_RIGHT 77
#define K_ENTER 28

int getkey(void);

void keyboard_init(void);

void write_key(u32 keycode);

int wait_key();

#endif