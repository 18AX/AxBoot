#include "keyboard.h"

#include "init/interrupts/idt.h"

static int keys_queue[KEY_QUEUE_SIZE];
int rear = -1;
u32 items_count = 0;

int getkey(void)
{
    if (items_count == 0)
    {
        return -1;
    }

    int tmp = rear;

    rear--;
    items_count--;
    if (rear < 0 && items_count != 0)
    {
        rear = KEY_QUEUE_SIZE - 1;
    }
    return keys_queue[tmp];
}

void keyboard_init(void)
{
    enable_irqA(1);
}

void write_key(u32 keycode)
{
    if ((keycode & 0x80) == 0x80)
    {
        return;
    }
    rear++;
    if (rear >= KEY_QUEUE_SIZE)
    {
        rear = 0;
    }
    keys_queue[rear] = keycode;
    items_count++;
}

int wait_key()
{
    int key = -1;
    while (((key = getkey()) == -1))
        ;

    return key;
}