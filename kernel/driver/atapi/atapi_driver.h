#ifndef ATAPI_HEADER
#define ATAPI_HEADER

#include "k/types.h"

#define ATA_CONTROLLER_1 0x1F0
#define ATA_CONTROLLER_2 0x170

#define DATA_REGISTER_OFFSET 0
#define ERROR_REGISTER_OFFSET 1
#define FEATURES_REGISTER_OFFSET 1
#define SECTOR_COUNT_OFFSET 2
#define LBA_LOW_OFFSET 3
#define LBA_MID_OFFSET 4
#define LBA_HIGH_OFFSET 5
#define DRIVE_REGISTER_OFFSET 6
#define STATUS_REGISTER_OFFSET 7
#define COMMAND_REGISTER_OFFSET 7

#define IDENTIFY_CMD 0xEC

#define SOFTWARE_RESET 0x4
// Stop sending interupts
#define NIEN 0x2

#define DCR_1 0x3F6
#define DCR_2 0x376

#define SECTOR_SIZE 2048

#define DATA_TRANSMIT 0x2
#define PACKET_COMMAND_COMPLETE 0x3

int check_atapi(u16 bus, u16 disk);

int atapi_init(void);

int atapi_read_sector(char *buffer, u32 sector);

int atapi_read(char *buffer, u32 sector, u32 offset, u32 len);

void wait_400(void);

u16 atapi_current_bus(void);

u16 atapi_current_disk(void);

#endif