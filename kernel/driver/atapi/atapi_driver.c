#include "atapi_driver.h"

#include "io/io.h"

static u16 current_bus = 0;
static u16 current_disk = 0;

void wait_400(void)
{
    inb(DCR_1);
    inb(DCR_1);
    inb(DCR_1);
    inb(DCR_1);
}

int check_atapi(u16 bus, u16 disk)
{
    outb(bus + DRIVE_REGISTER_OFFSET, disk);
    wait_400();

    u8 sector = inb(bus + SECTOR_COUNT_OFFSET);
    u8 lba_low = inb(bus + LBA_LOW_OFFSET);
    u8 lba_mid = inb(bus + LBA_MID_OFFSET);
    u8 lba_heigh = inb(bus + LBA_HIGH_OFFSET);

    return (sector & 0x01) != 0 && (lba_low & 0x01) != 0
        && (lba_mid & 0x14) != 0 && (lba_heigh & 0xEB) != 0;
}

int atapi_init(void)
{
    if (inb(ATA_CONTROLLER_1 + STATUS_REGISTER_OFFSET) == 0xFF
        || inb(ATA_CONTROLLER_2 + STATUS_REGISTER_OFFSET) == 0xFF)
    {
        return 0;
    }
    // Software reset + desable interrupts
    outb(DCR_1, SOFTWARE_RESET);
    outb(DCR_2, SOFTWARE_RESET);

    outb(DCR_1, NIEN);
    outb(DCR_2, NIEN);

    // Detect if disk 1 from bus 1 is atapi
    if (check_atapi(ATA_CONTROLLER_1, 0xA0))
    {
        current_bus = ATA_CONTROLLER_1;
        current_disk = 0xA0;
        return 1;
    }

    // Detect if disk 2 from bus 1 is atapi
    if (check_atapi(ATA_CONTROLLER_1, 0xB0))
    {
        current_bus = ATA_CONTROLLER_1;
        current_disk = 0xB0;
        return 1;
    }

    // Detect if disk 1 from bus 2 is atapi
    if (check_atapi(ATA_CONTROLLER_2, 0xA0))
    {
        current_bus = ATA_CONTROLLER_2;
        current_disk = 0xA0;
        return 1;
    }

    // Detect if disk 2 from bus 2 is atapi
    if (check_atapi(ATA_CONTROLLER_2, 0xB0))
    {
        current_bus = ATA_CONTROLLER_2;
        current_disk = 0xB0;
        return 1;
    }

    return 0;
}

int atapi_read_sector(char *buffer, u32 lba)
{
    if (current_bus == 0)
    {
        return -1;
    }

    u16 status;

    // Busy wait
    while ((status = inb(current_bus + STATUS_REGISTER_OFFSET) & 0x80))
        ;

    outb(current_bus + FEATURES_REGISTER_OFFSET, 0);
    outb(current_bus + SECTOR_COUNT_OFFSET, 0);
    outb(current_bus + LBA_MID_OFFSET, SECTOR_SIZE & 0xFF);
    outb(current_bus + LBA_HIGH_OFFSET, SECTOR_SIZE >> 8);
    outb(current_bus + COMMAND_REGISTER_OFFSET, 0xA0);

    // Wait packet
    status = 0;
    while ((status & 0x80) != 0 && (status & 0x08) == 0)
    {
        status = inb(current_bus + STATUS_REGISTER_OFFSET);
    }

    if ((status & 0x1) != 0)
    {
        return -1;
    }

    u8 cmd[12] = { 0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    cmd[9] = 1;
    cmd[1] = 0x18;
    cmd[2] = (lba >> 0x18) & 0xFF;
    cmd[3] = (lba >> 0x10) & 0xFF;
    cmd[4] = (lba >> 0x08) & 0xFF;
    cmd[5] = (lba >> 0x00) & 0xFF;

    u16 tmp = 0;

    for (u32 i = 0; i < 12; i += 2)
    {
        tmp = cmd[i + 1] << 8 | cmd[i];
        outw(current_bus + DATA_REGISTER_OFFSET, tmp);
    }

    wait_400();
    while (inb(current_bus + SECTOR_COUNT_OFFSET) != DATA_TRANSMIT)
    {
        wait_400();
    }

    u16 data = 0;

    for (u32 i = 0; i < SECTOR_SIZE; i += 2)
    {
        data = inw(current_bus + DATA_REGISTER_OFFSET);

        buffer[i] = data & 0xFF;
        buffer[i + 1] = data >> 8 & 0xFF;
    }

    while (inb(current_bus + SECTOR_COUNT_OFFSET) != PACKET_COMMAND_COMPLETE)
    {
        wait_400();
    }

    return SECTOR_SIZE;
}

int atapi_read(char *buffer, u32 lba, u32 offset, u32 len)
{
    if (current_bus == 0)
    {
        return -1;
    }

    u16 status;

    // Busy wait
    while ((status = inb(current_bus + STATUS_REGISTER_OFFSET) & 0x80))
        ;

    outb(current_bus + FEATURES_REGISTER_OFFSET, 0);
    outb(current_bus + SECTOR_COUNT_OFFSET, 0);
    outb(current_bus + LBA_MID_OFFSET, SECTOR_SIZE & 0xFF);
    outb(current_bus + LBA_HIGH_OFFSET, SECTOR_SIZE >> 8);
    outb(current_bus + COMMAND_REGISTER_OFFSET, 0xA0);

    // Wait packet
    status = 0;
    while ((status & 0x80) != 0 && (status & 0x08) == 0)
    {
        status = inb(current_bus + STATUS_REGISTER_OFFSET);
    }

    if ((status & 0x1) != 0)
    {
        return -1;
    }

    u8 cmd[12] = { 0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    cmd[9] = 1;
    cmd[1] = 0x18;
    cmd[2] = (lba >> 0x18) & 0xFF;
    cmd[3] = (lba >> 0x10) & 0xFF;
    cmd[4] = (lba >> 0x08) & 0xFF;
    cmd[5] = (lba >> 0x00) & 0xFF;

    u16 tmp = 0;

    for (u32 i = 0; i < 12; i += 2)
    {
        tmp = cmd[i + 1] << 8 | cmd[i];
        outw(current_bus + DATA_REGISTER_OFFSET, tmp);
    }

    wait_400();
    while (inb(current_bus + SECTOR_COUNT_OFFSET) != DATA_TRANSMIT)
    {
        wait_400();
    }

    u16 data = 0;

    u32 read = 0;

    for (u32 i = 0; i < SECTOR_SIZE; i += 2)
    {
        data = inw(current_bus + DATA_REGISTER_OFFSET);

        if (i >= offset && read < len)
        {
            buffer[read] = data & 0xFF;
            ++read;
        }

        if (i + 1 >= offset && read < len)
        {
            buffer[read] = data >> 8 & 0xFF;
            ++read;
        }
    }

    while (inb(current_bus + SECTOR_COUNT_OFFSET) != PACKET_COMMAND_COMPLETE)
    {
        wait_400();
    }

    return read;
}

u16 atapi_current_bus(void)
{
    return current_bus;
}

u16 atapi_current_disk(void)
{
    return current_disk;
}