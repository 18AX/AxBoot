#include "allocator.h"
#include "k/log.h"

static struct mem_status mem_status = { NULL, NULL, 0, NULL };

static void cache_init(void *high_address, u32 elt)
{
    char *ptr = ((char*)high_address) - (sizeof(struct list) * elt);

    struct cache_list *tmp = NULL;

    for (u32 i = 0; i < elt; ++i)
    {
        struct cache_list *c = (struct cache_list*) ptr;

        c->next = tmp;

        tmp = c;

        ptr += sizeof(struct list);
    }

    mem_status.cache = tmp;
    mem_status.nbr_cache_elt = elt;
}

static int cache_expand(u32 elt)
{
    // Let's find if the block before the cache is free

    struct list *current = mem_status.memory;
    u8 flag = 0;

    while (current != NULL)
    {
        if (current->base_ptr + current->len == mem_status.max_addr)
        {
            flag = 1;
            break;
        }

        current = current->next;
    }

    if (!flag || current == NULL || current->is_free != 1
        || current->len < sizeof(struct list) * elt)
    {
        return 0;
    }

    current->len -= sizeof(struct list) * elt;

    // We can expand the cache size

    char *ptr = mem_status.max_addr - (sizeof(struct list) * elt);
    mem_status.max_addr = ptr;

    struct cache_list *tmp = mem_status.cache;

    for (u32 i = 0; i < elt; ++i)
    {
        struct cache_list *c = (struct cache_list*) ptr;
        c->next = tmp;

        tmp = c;

        ptr += sizeof(struct list);
    }

    mem_status.cache = tmp;
    mem_status.nbr_cache_elt += elt;

    return 1;
}

static struct list *cache_alloc()
{
    if (mem_status.cache == NULL)
    {
        if (cache_expand(10) != 1)
        {
            return NULL;
        }
    }

    struct list *list = (struct list*) mem_status.cache;

    mem_status.cache = mem_status.cache->next;

    mem_status.nbr_cache_elt--;

    return list;
}

static void cache_free(struct list *list)
{
    struct cache_list *c = (struct cache_list*) list;

    c->next = mem_status.cache->next;

    mem_status.cache = c;

    mem_status.nbr_cache_elt++;
}

static void *__allocate_at(void *p, u32 size, u8 type)
{
    char *ptr = p;
    struct list *current = mem_status.memory;

    while (current != NULL)
    {
        if (current->is_free == 1 && current->base_ptr <= ptr
            && (current->base_ptr + current->len >= (ptr + size)))
        {
            if (current->base_ptr != p)
            {
                struct list *n = cache_alloc();

                if (n == NULL)
                {
                    return ALLOCATOR_FAILED;
                }

                n->base_ptr = current->base_ptr;
                n->len = ptr - current->base_ptr;
                n->next = current;
                n->prev = current->prev;
                n->is_free = 1;

                if (current->prev != NULL)
                {
                    current->prev->next = n;
                }

                current->prev = n;
            }

            if (ptr + size < current->base_ptr + current->len)
            {
                struct list *n = cache_alloc();

                if (n == NULL)
                {
                    return ALLOCATOR_FAILED;
                }

                n->base_ptr = ptr + size;
                n->len = (current->base_ptr + current->len) - (ptr + size);
                n->is_free = 1;
                n->prev = current;
                n->next = current->next;

                if (current->next != NULL)
                {
                    current->next->prev = n;
                }

                current->next = n;
            }

            current->base_ptr = ptr;
            current->len = size;
            current->is_free = type;

            if (current == mem_status.memory && current->prev != NULL)
            {
                mem_status.memory = current->prev;
            }

            return current->base_ptr;
        }

        current = current->next;
    }

    return ALLOCATOR_FAILED;
}

extern void *_end[];

void allocator_init(struct e820_entry *entry, u32 size)
{
    // Lets find the higher max address to put the metada
    char *max_addr = NULL;
    for (u32 i = 0; i < size; ++i)
    {
        if (entry[i].Type != 1)
        {
            continue;
        }

        // We are only have 4G
        char* base = (char*) entry[i].BaseL;
        u32 len = entry[i].LengthL;

        char* max = base + len;

        if (max > max_addr)
        {
            max_addr = max;
        }
    }

    cache_init(max_addr, ALLOCATOR_INIT_SIZE);

    struct list *metadata = cache_alloc();

    u32 metadata_size = ALLOCATOR_INIT_SIZE * sizeof(struct list);

    metadata->base_ptr = max_addr - metadata_size;
    metadata->is_free = 2;
    metadata->next = NULL;
    metadata->len = metadata_size;
    metadata->prev = NULL;
    struct list *init = cache_alloc();

    init->base_ptr = 0x0;
    init->is_free = 1;
    init->len = (u32) metadata->base_ptr;
    init->prev = NULL;
    init->next = metadata;

    metadata->prev = init;

    mem_status.memory = init;

    for (u32 i = 0; i < size; ++i)
    {
        if (entry[i].Type == 2)
        {
            if (entry[i].BaseH != 0)
            {
                continue;
            }
            // We only need 4 Go
            char* base = (char*) entry[i].BaseL;

            u32 len = entry[i].LengthH == 0 ? entry[i].LengthL : X86_MAX_ADDRESS;

            if (base > max_addr)
            {
                continue;
            }

            __allocate_at(base, len, 2);
        }
    }

    __allocate_at(0x0, (u32)(((char *)_end)) + 2048, 2);
}

void *allocate(u32 size)
{
    struct list *current = mem_status.memory;

    while (current != NULL)
    {
        if (current->is_free == 1 && current->len >= size)
        {
            if (current->len != size)
            {
                struct list *n = cache_alloc();
                if (n == NULL)
                {
                    return ALLOCATOR_FAILED;
                }
                n->base_ptr = current->base_ptr + size;
                n->len = current->len - size;
                n->prev = current;
                n->next = current->next;
                n->is_free = 1;
                current->next = n;
            }

            current->len = size;
            current->is_free = 0;

            return current->base_ptr;
        }

        current = current->next;
    }

    return ALLOCATOR_FAILED;
}

void *allocate_at(void *p, u32 size)
{
    return __allocate_at(p, size, 0);
}

void free_all_memory()
{
    struct list *current = mem_status.memory;

    while (current != NULL)
    {
        if (current->is_free == 0)
        {
            free(current->base_ptr);
        }

        current = current->next;
    }
}

void free(void *ptr)
{
    struct list *current = mem_status.memory;

    while (current != NULL)
    {
        if (current->base_ptr == ptr)
        {
            current->is_free = 1;

            struct list *prev = current->prev;
            struct list *next = current->next;

            if (prev != NULL && prev->is_free == 1)
            {
                current->base_ptr = prev->base_ptr;
                current->len += prev->len;

                current->prev = prev->prev;

                if (prev->prev != NULL)
                {
                    current->prev->next = current;
                }

                if (current->prev == NULL)
                {
                    mem_status.memory = current;
                }

                cache_free(prev);
            }

            if (next != NULL && next->is_free == 1)
            {
                current->len += next->len;
                current->next = next->next;

                if (current->next->prev != NULL)
                {
                    current->next->prev = current;
                }

                cache_free(next);
            }

            return;
        }

        current = current->next;
    }
}

void memory_dump()
{
    struct list *current = mem_status.memory;

    PRINTF("Memory dump :\n\n");

    while (current != NULL)
    {
        PRINTF("base = %x len = %x free = %d cache = %p prev = %p next %p\n",
               current->base_ptr, current->len, current->is_free, current,
               current->prev, current->next);
        current = current->next;
    }

    PRINTF("\ncache free status %lu\n", mem_status.nbr_cache_elt);
}

void cache_dump()
{
    PRINTF("Cache dump\n\n");

    PRINTF("Cache low address %lx\n", mem_status.max_addr);

    PRINTF("Free list : \n");

    struct cache_list *current = mem_status.cache;

    while (current != NULL)
    {
        PRINTF("%p\n", current);
        current = current->next;
    }

    PRINTF("\n");
}