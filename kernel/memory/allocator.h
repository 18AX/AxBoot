#ifndef ALLOCATOR_HEADER
#define ALLOCATOR_HEADER

#include "k/e820.h"

#define ALLOCATOR_FAILED 0x0

#define ALLOCATOR_INIT_SIZE 0x20

#define X86_MAX_ADDRESS 0xFFFFFFFF

struct list
{
    char *base_ptr;
    u32 len;
    u8 is_free;
    struct list *prev;
    struct list *next;
};

struct cache_list
{
    struct cache_list *next;
};

struct mem_status
{
    struct list *memory;
    struct cache_list *cache;
    u32 nbr_cache_elt;
    char *max_addr;
};

void allocator_init(struct e820_entry *entry, u32 size);

void *allocate(u32 size);

void *allocate_at(void *ptr, u32 size);

void free(void *ptr);

void free_all_memory();

void memory_dump();

#if TEST

struct mem_status* get_memstatus();

#endif

#endif