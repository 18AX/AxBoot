#ifndef CONFIG_HEADER
#define CONFIG_HEADER

#include "k/types.h"

struct config_elt
{
    char kernel_path[0xFF];
    char init_path[0xFF];
    char name[0xFF];
    struct config_elt *next;
};

struct config
{
    struct config_elt *elements;
    u32 size;
};

int read_conf(struct config *config);

void dump_config(struct config *config);

void free_conf(struct config *config);

struct config_elt *get_elt(struct config *config, u32 pos);

#endif