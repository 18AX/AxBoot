#include "config/config.h"

#if TEST
#    include <fcntl.h>
#    include <stdio.h>
#    include <stdlib.h>
#    include <sys/stat.h>
#    include <sys/types.h>

#    include "unistd.h"
#    include "utils/string.h"
#else
#    include "fs/file.h"
#    include "memory/allocator.h"
#    include "k/log.h"
#    include "utils/string.h"
#endif

#if TEST
static void *(*allocate)(unsigned long) = malloc;
static off_t (*seek)(int fd, off_t offset, int whence) = lseek;
#endif

char *skip_char_eq(char *str, char c)
{
    while (*str != '\0' && *str == c)
    {
        ++str;
    }

    return str;
}

char *skip_to_char(char *str, char c)
{
    while (*str != '\0' && *str != c)
    {
        ++str;
    }

    return str;
}

static void insert_element(struct config *config, struct config_elt *elt)
{
    if (elt == NULL || config == NULL)
    {
        return;
    }
    if (config->elements == NULL)
    {
        config->elements = elt;
        config->size = 1;

        elt->next = NULL;

        return;
    }

    struct config_elt *cur = config->elements;

    while (cur->next != NULL)
    {
        cur = cur->next;
    }

    cur->next = elt;
    config->size++;
}

static void parse_elt(char *str, struct config_elt *elt)
{
    char *middle = skip_to_char(str, '=');

    if (*middle == '\0')
    {
        return;
    }

    char tmp = *middle;
    *middle = '\0';

    if (strcmp(str, "kernel") == 0)
    {
        strcpy(elt->kernel_path, middle + 1);
        // strupper(elt->kernel_path);
    }

    if (strcmp(str, "init") == 0)
    {
        strcpy(elt->init_path, middle + 1);
        // strupper(elt->init_path);
    }

    if (strcmp(str, "name") == 0)
    {
        strcpy(elt->name, middle + 1);
    }

    *middle = tmp;
}

static struct config_elt *parse_line(char *line)
{
    struct config_elt *elt =
        (struct config_elt *)allocate(sizeof(struct config_elt));

    if (elt == NULL)
    {
        return NULL;
    }

    elt->init_path[0] = '\0';
    elt->kernel_path[0] = '\0';
    elt->name[0] = '\0';
    elt->next = NULL;

    line = skip_char_eq(line, ' ');

    int flag = 0;

    while (*line != '\0')
    {
        char *end = skip_to_char(line, ' ');

        char tmp = *end;

        *end = '\0';

        parse_elt(line, elt);

        if (strlen(elt->kernel_path) != 0 && flag == 0)
        {
            flag = 1;
        }

        *end = tmp;

        line = end;

        if (tmp != '\0')
        {
            line += 1;
        }
    }

    if (flag == 0)
    {
        free(elt);
        return NULL;
    }

    return elt;
}

int read_conf(struct config *config)
{
    int fd = open("boot.cfg");

    if (fd == -1)
    {
        return 0;
    }

    u32 file_size = seek(fd, 0, SEEK_END);

    seek(fd, 0, SEEK_SET);

    char *buffer = (char *)allocate(file_size + 1);

    if (buffer == NULL)
    {
        return 0;
    }

    if (read(fd, buffer, file_size) <= 0)
    {
        return 0;
    }

    buffer[file_size] = '\0';

    close(fd);

    char *begin = buffer;
    char *end = begin;

    while (*end != '\0')
    {
        end = skip_to_char(begin, '\n');

        char tmp = *end;

        *end = '\0';

        struct config_elt *elt = parse_line(begin);

        *end = tmp;

        if (elt == NULL)
        {
            begin = end + 1;
            continue;
        }

        insert_element(config, elt);

        begin = end + 1;
    }

    free(buffer);

    return 1;
}

void dump_config(struct config *config)
{
    PRINTF("config {\n");

    struct config_elt *curr = config->elements;

    while (curr != NULL)
    {
        PRINTF("\tkernel: %s initrd: %s\n", curr->kernel_path, curr->init_path);
        curr = curr->next;
    }

    PRINTF("}\n");
}

void free_conf(struct config *config)
{
    struct config_elt *curr = config->elements;

    while (curr != NULL)
    {
        struct config_elt *tmp = curr;
        curr = curr->next;
        free(tmp);
    }
}

struct config_elt *get_elt(struct config *config, u32 pos)
{
    struct config_elt *curr = config->elements;

    u32 i = 0;

    while (curr != NULL)
    {
        if (i == pos)
        {
            return curr;
        }

        ++i;
        curr = curr->next;
    }

    return NULL;
}