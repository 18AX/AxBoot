#include "irq_handler.h"

#include "driver/keyboard/keyboard.h"
#include "io/io.h"
#include "k/log.h"
#include "utils/ui.h"

void irq_handler(int code)
{
    disable_interupt();
    send_eoi();

    if (code <= 0x1F)
    {
        die_with_error(code);
    }

    switch (code)
    {
    case KEYBOARD_CODE:
        if ((inb(KEYBOARD_STATUS_PORT) & 1) != 0)
        {
            write_key(inb(KEYBOARD_READ_PORT));
        }
        break;

    default:
        LOG_INFO("HANDLER 0x%x\n", code);

        break;
    }

    enable_interupts();
}
