#ifndef IRQ_HANDLER
#define IRQ_HANDLER

#include "init/interrupts/idt.h"
#include "k/types.h"

#define PIT_CODE OFFSET_PIC1
#define KEYBOARD_CODE 0x1 + OFFSET_PIC1

void irq_handler(int code);

void set_handler(u32 index, void (*f)(void));

void reset_handler(u32 index);

#endif