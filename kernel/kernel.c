#include "config/config.h"
#include "draw/draw.h"
#include "draw/menu.h"
#include "draw/print.h"
#include "driver/atapi/atapi_driver.h"
#include "driver/keyboard/keyboard.h"
#include "init/interrupts/idt.h"
#include "k/bootinfo.h"
#include "k/e820.h"
#include "loader/linux.h"
#include "k/log.h"
#include "serial/serial.h"
#include "utils/string.h"
#include "utils/ui.h"
#include "memory/allocator.h"

void init(struct boot_info *info)
{
    // init_serial();

    allocator_init(info->e820_ptr, info->e820_size);
    memory_dump();
    draw_init(info->vbe_mode_info_ptr);

    init_pic();
    init_idt();
    load_idt();
    enable_interupts();

    LOG_SUCCESS("interrupts set up");

    keyboard_init();

    LOG_INFO("trying to detect disk");

    if (atapi_init() == 0)
    {
        LOG_ERROR("cannot detect disks");
        die_with_error_message("Unsupported drive");
    }

    LOG_SUCCESS("found one atapi disk bus %x disk %x", atapi_current_bus(),
                atapi_current_disk());
}

void main(struct boot_info *info)
{
    LOG_SUCCESS("Welcome to bootlaoder");

    init(info);

    LOG_SUCCESS("Init success");

    struct config config = { NULL, 0 };

    if (read_conf(&config) != 1)
    {
        die_with_error_message("Cannot read config file");
    }

    struct menu *menu =
        create_menu_from_config(info->vbe_mode_info_ptr, &config);

    if (menu == NULL)
    {
        die_with_error_message("Cannot allocate the menu");
    }

    s32 selected_item = draw_menu(menu);

    if (selected_item == -1)
    {
        die_with_error_message("Broken config file");
    }

    struct config_elt *elt = get_elt(&config, selected_item);

    if (elt == NULL)
    {
        die_with_error_message("Broken config file");
    }

    clear_screen((struct rgb){ 0x0, 0x0, 0x0 });

    print("Loading your kernel in memory...", 0, 0,
          (struct rgb){ 0xFF, 0xFF, 0xFF });

    update_screen();

    char kernel_path[0xFF];
    char init_path[0xFF];

    strcpy(kernel_path, elt->kernel_path);
    strcpy(init_path, elt->init_path);

    LOG_INFO("will boot kernel %s", kernel_path);

    free_all_memory();

    if (boot_linux(kernel_path, init_path, info) != 1)
    {
        LOG_ERROR("Cannot boot linux");
    }

    for (;;)
    {
        __asm__ volatile("hlt");
    }
}