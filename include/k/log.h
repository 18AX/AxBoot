#ifndef LOGGER_HEADER
#define LOGGER_HEADER

#ifdef DEBUG
#    include "k/stdarg.h"
#    include "k/types.h"
#    define LOG_INFO(fmt, ...) log_info(fmt, ##__VA_ARGS__)
#    define LOG_ERROR(fmt, ...) log_error(fmt, ##__VA_ARGS__)
#    define LOG_SUCCESS(fmt, ...) log_success(fmt, ##__VA_ARGS__)
#    define PRINTF(fmt, ...) printf(fmt, ##__VA_ARGS__)


void log_info(const char *txt, ...);

void log_success(const char *txt, ...);

void log_error(const char *txt, ...);

int printf(const char *fmt, ...);

int puts(const char *s);

#else
#    define LOG_INFO(a, ...)
#    define LOG_ERROR(b, ...)
#    define LOG_SUCCESS(c, ...)
#    define PRINTF(d, ...)
#endif

#endif