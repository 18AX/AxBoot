#ifndef BOOT_PARAMS
#define BOOT_PARAMS

#include "k/types.h"

#define EDDNR 0x1e9		/* addr of number of edd_info structs at EDDBUF
				   in boot_params - treat this as 1 byte  */
#define EDDBUF	0xd00		/* addr of edd_info structs in boot_params */
#define EDDMAXNR 6		/* number of edd_info structs starting at EDDBUF  */
#define EDDEXTSIZE 8		/* change these if you muck with the structures */
#define EDDPARMSIZE 74
#define CHECKEXTENSIONSPRESENT 0x41
#define GETDEVICEPARAMETERS 0x48
#define LEGACYGETDEVICEPARAMETERS 0x08
#define EDDMAGIC1 0x55AA
#define EDDMAGIC2 0xAA55


#define READ_SECTORS 0x02         /* int13 AH=0x02 is READ_SECTORS command */
#define EDD_MBR_SIG_OFFSET 0x1B8  /* offset of signature in the MBR */
#define EDD_MBR_SIG_BUF    0x290  /* addr in boot params */
#define EDD_MBR_SIG_MAX 16        /* max number of signatures to store */
#define EDD_MBR_SIG_NR_BUF 0x1ea  /* addr of number of MBR signtaures at EDD_MBR_SIG_BUF in boot_params - treat this as 1 byte  */

#define E820_MAX_ENTRIES_ZEROPAGE 128

struct setup_header {
	u8	setup_sects;
	u16	root_flags;
	u32	syssize;
	u16	ram_size;
	u16	vid_mode;
	u16	root_dev;
	u16	boot_flag;
	u16	jump;
	u32	header;
	u16	version;
	u32	realmode_swtch;
	u16	start_sys_seg;
	u16	kernel_version;
	u8	type_of_loader;
	u8	loadflags;
	u16	setup_move_size;
	u32	code32_start;
	u32	ramdisk_image;
	u32	ramdisk_size;
	u32	bootsect_kludge;
	u16	heap_end_ptr;
	u8	ext_loader_ver;
	u8	ext_loader_type;
	u32	cmd_line_ptr;
	u32	initrd_addr_max;
	u32	kernel_alignment;
	u8	relocatable_kernel;
	u8	min_alignment;
	u16	xloadflags;
	u32	cmdline_size;
	u32	hardware_subarch;
	u64	hardware_subarch_data;
	u32	payload_offset;
	u32	payload_length;
	u64	setup_data;
	u64	pref_address;
	u32	init_size;
	u32	handover_offset;
	u32	kernel_info_offset;
} __attribute__((packed));

struct edd_device_params {
	u16 length;
	u16 info_flags;
	u32 num_default_cylinders;
	u32 num_default_heads;
	u32 sectors_per_track;
	u64 number_of_sectors;
	u16 bytes_per_sector;
	u32 dpte_ptr;		/* 0xFFFFFFFF for our purposes */
	u16 key;		/* = 0xBEDD */
	u8 device_path_info_length;	/* = 44 */
	u8 reserved2;
	u16 reserved3;
	u8 host_bus_type[4];
	u8 interface_type[8];
	union {
		struct {
			u16 base_address;
			u16 reserved1;
			u32 reserved2;
		} __attribute__  ((packed)) isa;
		struct {
			u8 bus;
			u8 slot;
			u8 function;
			u8 channel;
			u32 reserved;
		} __attribute__  ((packed)) pci;
		/* pcix is same as pci */
		struct {
			u64 reserved;
		} __attribute__  ((packed)) ibnd;
		struct {
			u64 reserved;
		} __attribute__  ((packed)) xprs;
		struct {
			u64 reserved;
		} __attribute__  ((packed)) htpt;
		struct {
			u64 reserved;
		} __attribute__  ((packed)) unknown;
	} interface_path;
	union {
		struct {
			u8 device;
			u8 reserved1;
			u16 reserved2;
			u32 reserved3;
			u64 reserved4;
		} __attribute__  ((packed)) ata;
		struct {
			u8 device;
			u8 lun;
			u8 reserved1;
			u8 reserved2;
			u32 reserved3;
			u64 reserved4;
		} __attribute__  ((packed)) atapi;
		struct {
			u16 id;
			u64 lun;
			u16 reserved1;
			u32 reserved2;
		} __attribute__  ((packed)) scsi;
		struct {
			u64 serial_number;
			u64 reserved;
		} __attribute__  ((packed)) usb;
		struct {
			u64 eui;
			u64 reserved;
		} __attribute__  ((packed)) i1394;
		struct {
			u64 wwid;
			u64 lun;
		} __attribute__  ((packed)) fibre;
		struct {
			u64 identity_tag;
			u64 reserved;
		} __attribute__  ((packed)) i2o;
		struct {
			u32 array_number;
			u32 reserved1;
			u64 reserved2;
		} __attribute__  ((packed)) raid;
		struct {
			u8 device;
			u8 reserved1;
			u16 reserved2;
			u32 reserved3;
			u64 reserved4;
		} __attribute__  ((packed)) sata;
		struct {
			u64 reserved1;
			u64 reserved2;
		} __attribute__ ((packed)) unknown;
	} device_path;
	u8 reserved4;
	u8 checksum;
} __attribute__ ((packed));

struct edd_info {
	u8 device;
	u8 version;
	u16 interface_support;
	u16 legacy_max_cylinder;
	u8 legacy_max_head;
	u8 legacy_sectors_per_track;
	struct edd_device_params params;
} __attribute__ ((packed));

struct edd {
	unsigned int mbr_signature[EDD_MBR_SIG_MAX];
	struct edd_info edd_info[EDDMAXNR];
	unsigned char mbr_signature_nr;
	unsigned char edd_info_nr;
};

struct edid_info {
	unsigned char dummy[128];
};

struct apm_bios_info {
	u16	version;
	u16	cseg;
	u32	offset;
	u16	cseg_16;
	u16	dseg;
	u16	flags;
	u16	cseg_len;
	u16	cseg_16_len;
	u16	dseg_len;
};

struct screen_info {
	u8  orig_x;		/* 0x00 */
	u8  orig_y;		/* 0x01 */
	u16 ext_mem_k;	/* 0x02 */
	u16 orig_video_page;	/* 0x04 */
	u8  orig_video_mode;	/* 0x06 */
	u8  orig_video_cols;	/* 0x07 */
	u8  flags;		/* 0x08 */
	u8  unused2;		/* 0x09 */
	u16 orig_video_ega_bx;/* 0x0a */
	u16 unused3;		/* 0x0c */
	u8  orig_video_lines;	/* 0x0e */
	u8  orig_video_isVGA;	/* 0x0f */
	u16 orig_video_points;/* 0x10 */

	/* VESA graphic mode -- linear frame buffer */
	u16 lfb_width;	/* 0x12 */
	u16 lfb_height;	/* 0x14 */
	u16 lfb_depth;	/* 0x16 */
	u32 lfb_base;		/* 0x18 */
	u32 lfb_size;		/* 0x1c */
	u16 cl_magic, cl_offset; /* 0x20 */
	u16 lfb_linelength;	/* 0x24 */
	u8  red_size;		/* 0x26 */
	u8  red_pos;		/* 0x27 */
	u8  green_size;	/* 0x28 */
	u8  green_pos;	/* 0x29 */
	u8  blue_size;	/* 0x2a */
	u8  blue_pos;		/* 0x2b */
	u8  rsvd_size;	/* 0x2c */
	u8  rsvd_pos;		/* 0x2d */
	u16 vesapm_seg;	/* 0x2e */
	u16 vesapm_off;	/* 0x30 */
	u16 pages;		/* 0x32 */
	u16 vesa_attributes;	/* 0x34 */
	u32 capabilities;     /* 0x36 */
	u32 ext_lfb_base;	/* 0x3a */
	u8  _reserved[2];	/* 0x3e */
} __attribute__((packed));

struct ist_info {
	u32 signature;
	u32 command;
	u32 event;
	u32 perf_level;
};

struct sys_desc_table {
	u16 length;
	u8  table[14];
};

struct olpc_ofw_header {
	u32 ofw_magic;	/* OFW signature */
	u32 ofw_version;
	u32 cif_handler;	/* callback into OFW */
	u32 irq_desc_table;
} __attribute__((packed));

struct efi_info {
	u32 efi_loader_signature;
	u32 efi_systab;
	u32 efi_memdesc_size;
	u32 efi_memdesc_version;
	u32 efi_memmap;
	u32 efi_memmap_size;
	u32 efi_systab_hi;
	u32 efi_memmap_hi;
};

struct boot_e820_entry {
	u64 addr;
	u64 size;
	u32 type;
} __attribute__((packed));

struct boot_params {
	struct screen_info screen_info;			/* 0x000 */
	struct apm_bios_info apm_bios_info;		/* 0x040 */
	u8  _pad2[4];					/* 0x054 */
	u64  tboot_addr;				/* 0x058 */
	struct ist_info ist_info;			/* 0x060 */
	u64 acpi_rsdp_addr;				/* 0x070 */
	u8  _pad3[8];					/* 0x078 */
	u8  hd0_info[16];	/* obsolete! */		/* 0x080 */
	u8  hd1_info[16];	/* obsolete! */		/* 0x090 */
	struct sys_desc_table sys_desc_table; /* obsolete! */	/* 0x0a0 */
	struct olpc_ofw_header olpc_ofw_header;		/* 0x0b0 */
	u32 ext_ramdisk_image;			/* 0x0c0 */
	u32 ext_ramdisk_size;				/* 0x0c4 */
	u32 ext_cmd_line_ptr;				/* 0x0c8 */
	u8  _pad4[116];				/* 0x0cc */
	struct edid_info edid_info;			/* 0x140 */
	struct efi_info efi_info;			/* 0x1c0 */
	u32 alt_mem_k;				/* 0x1e0 */
	u32 scratch;		/* Scratch field! */	/* 0x1e4 */
	u8  e820_entries;				/* 0x1e8 */
	u8  eddbuf_entries;				/* 0x1e9 */
	u8  edd_mbr_sig_buf_entries;			/* 0x1ea */
	u8  kbd_status;				/* 0x1eb */
	u8  secure_boot;				/* 0x1ec */
	u8  _pad5[2];					/* 0x1ed */
	/*
	 * The sentinel is set to a nonzero value (0xff) in header.S.
	 *
	 * A bootloader is supposed to only take setup_header and put
	 * it into a clean boot_params buffer. If it turns out that
	 * it is clumsy or too generous with the buffer, it most
	 * probably will pick up the sentinel variable too. The fact
	 * that this variable then is still 0xff will let kernel
	 * know that some variables in boot_params are invalid and
	 * kernel should zero out certain portions of boot_params.
	 */
	u8  sentinel;					/* 0x1ef */
	u8  _pad6[1];					/* 0x1f0 */
	struct setup_header hdr;    /* setup header */	/* 0x1f1 */
	u8  _pad7[0x290-0x1f1-sizeof(struct setup_header)];
	u32 edd_mbr_sig_buffer[EDD_MBR_SIG_MAX];	/* 0x290 */
	struct boot_e820_entry e820_table[E820_MAX_ENTRIES_ZEROPAGE]; /* 0x2d0 */
	u8  _pad8[48];				/* 0xcd0 */
	struct edd_info eddbuf[EDDMAXNR];		/* 0xd00 */
	u8  _pad9[276];				/* 0xeec */
} __attribute__((packed));

#endif