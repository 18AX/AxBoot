#ifndef EDID_HEADER
#define EDID_HEADER

#include "k/types.h"

struct edid
{
    u8 padding[8];
    u16 manufacture_id;
    u16 edid_code;
    u32 serial_number;

    u8 week;
    u8 year;

    u8 version;
    u8 revision;
    u8 video_type;

    u8 max_hor_size;
    u8 max_ver_size;

    u8 gama_factor;

    u8 dpms_flags;

    u8 chroma[10];

    u8 timings_1;
    u8 timings_2;

    u8 res_timings;
    u16 std_timing_id[8];

    u8 timing_desc_1[18];
    u8 timing_desc_2[18];
    u8 timing_desc_3[18];
    u8 timing_desc_4[18];

    u8 unused;
    u8 checksum;
} __attribute__((packed));

#endif