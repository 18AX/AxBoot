#ifndef VBE_HEADER
#define VBE_HEADER

#include "k/types.h"

struct vbe_info_structure {
	char signature[4];	// must be "VESA" to indicate valid VBE support
	u16 version;			// VBE version; high byte is major version, low byte is minor version
	u32 oem;			// segment:offset pointer to OEM
	u32 capabilities;		// bitfield that describes card capabilities
	u32 video_modes;		// segment:offset pointer to list of supported video modes
	u16 video_memory;		// amount of video memory in 64KB blocks
	u16 software_rev;		// software revision
	u32 vendor;			// segment:offset to card vendor string
	u32 product_name;		// segment:offset to card model name
	u32 product_rev;		// segment:offset pointer to product revision
	char reserved[222];		// reserved for future expansion
	char oem_data[256];		// OEM BIOSes store their strings in this area
} __attribute__ ((packed));

struct vbe_mode_info_structure {
	u16 attributes;		// deprecated, only bit 7 should be of interest to you, and it indicates the mode supports a linear frame buffer.
	u8 window_a;			// deprecated
	u8 window_b;			// deprecated
	u16 granularity;		// deprecated; used while calculating bank numbers
	u16 window_size;
	u16 segment_a;
	u16 segment_b;
	u32 win_func_ptr;		// deprecated; used to switch banks from protected mode without returning to real mode
	u16 pitch;			// number of bytes per horizontal line
	u16 width;			// width in pixels
	u16 height;			// height in pixels
	u8 w_char;			// unused...
	u8 y_char;			// ...
	u8 planes;
	u8 bpp;			// bits per pixel in this mode
	u8 banks;			// deprecated; total number of banks in this mode
	u8 memory_model;
	u8 bank_size;		// deprecated; size of a bank, almost always 64 KB but may be 16 KB...
	u8 image_pages;
	u8 reserved0;
	u8 red_mask;
	u8 red_position;
	u8 green_mask;
	u8 green_position;
	u8 blue_mask;
	u8 blue_position;
	u8 reserved_mask;
	u8 reserved_position;
	u8 direct_color_attributes;
	u32 framebuffer;		// physical address of the linear frame buffer; write here to draw to the screen
	u32 off_screen_mem_off;
	u16 off_screen_mem_size;	// size of memory in the framebuffer but not being displayed on the screen

    u16 lin_bytes_per_scanline;
    u8  banked_image_count;
    u8  lin_image_count;
    u8  lin_red_mask_size;
    u8  lin_red_mask_shift;
    u8  lin_green_mask_size;
    u8  lin_green_mask_shift;
    u8  lin_blue_mask_size;
    u8  lin_blue_mask_shift;
    u8  lin_rsvd_mask_size;
    u8  lin_rsvd_mask_shift;
    u32 max_pixel_clock;

    u8  reserved2[189];
} __attribute__ ((packed));

#endif