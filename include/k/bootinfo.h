#ifndef BOOT_INFO_HEADER
#define BOOT_INFO_HEADER

#include "k/e820.h"
#include "k/vbe.h"
#include "k/edid.h"

struct boot_info
{
    struct e820_entry *e820_ptr;
    u32 e820_size;

    struct vbe_info_structure *vbe_info;
    struct vbe_mode_info_structure *vbe_mode_info_ptr;

    struct edid *edid_ptr;
};

#endif