#ifndef STDIO_HEADER
#define STDIO_HEADER

#include "k/stdarg.h"
#include "k/types.h"

int vsprintf(char *buf, const char *fmt, va_list args);

int sprintf(char *buf, const char *fmt, ...);

#endif