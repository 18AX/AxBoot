#ifndef GDT_HEADER
#define GDT_HEADER

#include "k/types.h"

struct gdt_entry
{
    u16 limit0;
    u16 base0;
    u8 base1;
    u8 access;
    u8 limit1 : 4;
    u8 flag : 4;
    u8 base2;
} __attribute__((packed));

struct gdt_ptr
{
    u16 limit;
    u32 base_ptr;
} __attribute__((packed));

#endif