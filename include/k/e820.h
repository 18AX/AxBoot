#ifndef E820_HEADER
#define E820_HEADER

#define E820_SIZE 0x1000
#define E820_PTR 0x1004

#include "k/types.h"

struct e820_entry
{
    u32 BaseL; // base address uint64_t
	u32 BaseH;
	u32 LengthL; // length uint64_t
	u32 LengthH;
	u32 Type; // entry Type
	u32 ACPI; 
} __attribute__((packed));

#endif