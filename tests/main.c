#include <cgreen/cgreen.h>
#include "test_config.h"

int main(int argc, char *argv[])
{
    TestSuite *suite = create_test_suite();

    add_suite(suite, config_tests());

    return run_test_suite(suite, create_text_reporter());
}