#include "test_config.h"
#include "config/config.h"

Describe(Config);
BeforeEach(Config) {}
AfterEach(Config) {}

Ensure(Config, passes_this_test) {
    struct config conf = {NULL, 0};

    int res = read_conf(&conf);

    dump_config(&conf);

    assert_that(res == 1);

    assert_that(conf.elements != NULL);

    assert_string_equal(conf.elements->kernel_path, "BOOT/VMLINUZ");
    assert_string_equal(conf.elements->init_path, "BOOT/CORE.GZ");
    assert_that(conf.elements->next != NULL);

    assert_string_equal(conf.elements->next->kernel_path, "BOOT/FOO");
    assert_string_equal(conf.elements->next->init_path, "BOOT/DOO");
    assert_that(conf.elements->next != NULL);

    free_conf(&conf);
}

TestSuite *config_tests()
{
    TestSuite* suite = create_test_suite();

    add_test_with_context(suite, Config, passes_this_test);

    return suite;
}