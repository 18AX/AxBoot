megs: 32
romimage: file=/usr/share/bochs/BIOS-bochs-latest, address=0xfffe0000
vgaromimage: file=/usr/share/bochs/VGABIOS-lgpl-latest
vga: extension=vbe, update_freq=60

ata0: enabled=1, ioaddr1=0x1f0, ioaddr2=0x3f0, irq=14
#ata0-master: type=cdrom, path="core.iso", status=inserted
#boot: cdrom

#floppya: 1_44=boot/boot.bin, status=inserted
#boot: a

floppya: 1_44=boot.img, status=inserted
boot: a

com1: enabled=1, mode=file, dev=/dev/stdout

#ata0: enabled=1, ioaddr1=0x1f0, ioaddr2=0x3f0, irq=14
log: out.log


#display_library: x, options="gui_debug"
