#ifndef STRING_HEADER
#define STRING_HEADER

#include "k/types.h"

unsigned long strlen(const char *str);

u32 strnlen(const char *s, u32 maxlen);

void *memcpy(void *dest, const void *src, u32 n);

int strcmp(const char *s1, const char *s2);

void *memset(void *s, int c, u32 n);

char *strcpy(char *dest, const char *src);

#endif