#ifndef PRINT_HEADER
#define PRINT_HEADER

#if DEBUG
#    define PRINT(fmt) printstr(fmt)
void printstr(const char *str);

#else
#    define PRINT(e)

#endif

#endif