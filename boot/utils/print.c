#if DEBUG
#include "k/log.h"
#include "serial/write32.h"
#include "utils/string.h"

void printstr(const char *str)
{
    u32 size = strlen(str);
    write32(str, size);
}
#endif