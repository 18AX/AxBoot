
section .text

[BITS 16]
global switch_32
global get_vbe_info
global get_vbe_mode_info
global set_vbe_mode
global get_edid_info


get_vbe_info:
    push ebp
    mov ebp,esp

    push es
    push di

    mov di,[ebp+8]

    xor eax,eax

    mov ax,0x0
    mov es,ax

    mov ax,0x4F00

    int 0x10

    pop di
    pop es
    pop ebp
    ret

get_vbe_mode_info:
    push ebp
    mov ebp,esp

    push es
    push di

    mov di,[ebp+8]
    mov cx,[ebp+12]

    mov ax,0x0
    mov es,ax

    mov ax,0x4F01

    int 0x10

    pop di
    pop es
    pop ebp
    ret

get_edid_info:
    push ebp
    mov ebp,esp

    push es
    push di

    mov di,[ebp+8]

    xor eax,eax
    xor ebx,ebx
    xor ecx,ecx
    xor edx,edx

    mov ax,0x0
    mov es,ax

    mov ax, 0x4f15
    mov bl, 0x01

    int 0x10

    pop di
    pop es
    pop ebp
    ret

set_vbe_mode:
    push ebp
    mov ebp,esp

    mov bx,[ebp+8]

    mov ax,0x4F02

    int 0x10

    pop ebp
    ret

switch_32:

    lgdt [gdt_pointer] ; load lgdt to enable 4gb

    ;now enable SSE and the like
    mov eax, cr0
    and ax, 0xFFFB		;clear coprocessor emulation CR0.EM
    or ax, 0x2			;set coprocessor monitoring  CR0.MP
    mov cr0, eax
    mov eax, cr4
    or ax, 3 << 9		;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
    mov cr4, eax


    mov eax,cr0
    or eax,0x1
    mov cr0,eax ; set bit 0 to 1 which enable protected mode on the cpu
        

    jmp 0x10:start

[BITS 32]
start:
    mov ax, DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    extern entry32
    jmp entry32

gdt_start:
    dq 0x0
    dq 0x0
gdt_code:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10011010b
    db 11001111b
    db 0x0
gdt_data:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10010010b
    db 11001111b
    db 0x0
gdt_end:

gdt_pointer:
    dw gdt_end - gdt_start ; set the size of the gdt
    dd gdt_start ; set the address of the 
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

