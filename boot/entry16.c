__asm__(".code16gcc");

#include "k/bootinfo.h"
#include "k/bootparams.h"
#include "k/e820.h"
#include "k/edid.h"
#include "k/gdt.h"
#include "k/types.h"
#include "k/vbe.h"
#include "serial/serial.h"
#include "serial/write.h"

#define FUNC __attribute__((noinline)) __attribute__((regparm(3)))

#define FP_OFF(fp) ((u32)(fp)&0xFFFF)
#define FP_SEG(fp) ((u16)((u32)(fp) >> 16))

#define AL(nb) (nb & 0xFF)
#define AH(nb) ((nb >> 8) & 0xFF)
#define AX(nb) (nb & 0xFFFF)

/**
 * code from osdev
 */
s32 FUNC detect_memory(struct e820_entry *buffer, s32 maxentries)
{
    u32 contID = 0;
    s32 entries = 0, signature, bytes;
    do
    {
        __asm__ __volatile__("int  $0x15"
                             : "=a"(signature), "=c"(bytes), "=b"(contID)
                             : "a"(0xE820), "b"(contID), "c"(24),
                               "d"(0x534D4150), "D"(buffer));
        if (signature != 0x534D4150)
            return -1; // error
        if (bytes > 20 && (buffer->ACPI & 0x0001) == 0)
        {
            // ignore this entry
        }
        else
        {
            buffer++;
            entries++;
        }
    } while (contID != 0 && entries < maxentries);
    return entries;
}

void swap(char *a, char *b)
{
    char t = *a;
    *a = *b;
    *b = t;
}

void reverse(char str[], int length)
{
    int start = 0;
    int end = length - 1;
    while (start < end)
    {
        swap(str + start, str + end);
        start++;
        end--;
    }
}

extern u32 get_vbe_info(u16 di);

extern u32 get_vbe_mode_info(u16 di, u16 mode);

extern u32 set_vbe_mode(u16 value);

extern u32 get_edid_info(u16 di);

s32 FUNC vesa_init(struct vbe_info_structure *vbe_info,
                   struct vbe_mode_info_structure *info)
{
    vbe_info->signature[0] = 'V';
    vbe_info->signature[1] = 'E';
    vbe_info->signature[2] = 'S';
    vbe_info->signature[3] = 'A';

    u32 eax = get_vbe_info(FP_OFF(vbe_info));

    if (AX(eax) != 0x004F)
    {
        return -1;
    }

    u16 *video_modes = (u16 *)vbe_info->video_modes;

    for (u32 i = 0; video_modes[i] != 0xFFFF; ++i)
    {
        if (AL(get_vbe_mode_info(FP_OFF(info), video_modes[i])) != 0x004F)
        {
            continue;
        }

        if (info->memory_model != 0x6 || !(info->attributes & (1 << 7)))
        {
            continue;
        }

        if (info->width == 800 && info->height == 600 && info->bpp == 32)
        {
            u16 value = video_modes[i];

            value |= (1 << 14); // LFB set
            value &= ~(1 << 15); // clear screen

            return AX(set_vbe_mode(value)) == 0x004F;
        }
    }

    return -1;
}

extern void switch_32(struct boot_info *info);

void FUNC entry16()
{
#if DEBUG
    init_serial();

    write("Entering 16 bit\n", 16);

#endif

    struct e820_entry e820_entries[E820_MAX_ENTRIES_ZEROPAGE];

    struct vbe_info_structure vbe_info;
    struct vbe_mode_info_structure vbe_mode_info;

    struct edid edid;

    struct edid *edid_ptr = &edid;

    if (AH(get_edid_info(FP_OFF(edid_ptr))) == 1)
    {
#if DEBUG
        write("Edid failed\n", 12);
#endif
        edid_ptr = NULL;
    };

    if (vesa_init(&vbe_info, &vbe_mode_info) == -1)
    {
#if DEBUG
        write("VBE not supported\n", 18);
#endif
        __asm__ volatile("hlt");
    }

    s32 nbr_entry = detect_memory(e820_entries, E820_MAX_ENTRIES_ZEROPAGE);

    struct boot_info info = { e820_entries, nbr_entry, &vbe_info,
                              &vbe_mode_info, edid_ptr };

    switch_32(&info);
}