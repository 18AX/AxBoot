#ifndef IO_H_
#define IO_H_

#include <k/types.h>

static inline void outb(u16 port, u8 val)
{
    __asm__ volatile("outb %0, %1" : /* No output */ : "a"(val), "d"(port));
}

static inline u8 inb(u16 port)
{
    u8 res;

    __asm__ volatile("inb %1, %0" : "=&a"(res) : "d"(port));

    return res;
}

static inline void outw(u16 port, u16 val)
{
    __asm__ volatile("outw %0, %1" : /* No output */ : "a"(val), "d"(port));
}

static inline u16 inw(u16 port)
{
    u16 res;

    __asm__ volatile("inw %1, %0" : "=&a"(res) : "d"(port));

    return res;
}

#endif /* !IO_H_ */
