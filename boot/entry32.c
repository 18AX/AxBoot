#include "driver/atapi/atapi_driver.h"
#include "fs/file.h"
#include "k/bootinfo.h"
#include "k/types.h"
#include "utils/print.h"

#define KERNEL_ADDRESS 0x50000

void entry32(struct boot_info *info)
{
    PRINT("Entering 32\n");
    if (atapi_init() != 1)
    {
        PRINT("No atapi disk found\n");
        __asm__("hlt");
    }

    int fd = open("boot/kernel.bin");

    u32 size = seek(fd, 0, SEEK_END);

    seek(fd, 0, SEEK_SET);

    void *buffer = (void *)KERNEL_ADDRESS;

    if (read(fd, buffer, size) == -1)
    {
        PRINT("Kernel not found\n");
        for (;;)
        {
            __asm__("hlt");
        }
    }

    PRINT("Jumping on 32 bit kernel\n");

    void (*fun_ptr)(struct boot_info * info) =
        (void (*)(struct boot_info *))KERNEL_ADDRESS;

    fun_ptr(info);
}