section .boot

[BITS 16]
global boot

; entry point
; set up A20 line
boot:
    cli
    mov ax,2402h ; check if A20 is set
    int 15h
    cmp al,$1
    je a20_enable
    mov ax,2401h ; activate A20 gate
    int 15h
    
a20_enable:

; Read the next sector to load the kernel
    mov ah,02h
    mov al,0x11 ; we need to read 39 * 512 bytes
    mov ch,0x0
    mov dh,0x0
    mov cl,0x2 ; because our boot is on sector 1 so the kernel will be on sector 2
    
    mov bx,start
    int 13h
   
    jmp start
times 510-($-$$) db 0

db 0x55
db 0xaa

start:
    mov esp,code16_stack_top
    mov ebp,esp

    extern entry16
    call entry16
    cli
    hlt


section .bss
align 4
code16_stack_bottom: equ $
    resb 16384 ;
code16_stack_top: