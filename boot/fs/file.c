#include "file.h"

#include "driver/atapi/atapi_driver.h"
#include "iso/isoreader.h"

static struct file open_files[0xFF] = { 0 };

int open(char *pathname)
{
    u32 pos = 0;

    for (; pos < 0xFF; ++pos)
    {
        if (open_files[pos].open != 1)
        {
            break;
        }
    }

    if (iso_find_file(pathname, &open_files[pos]) == -1)
    {
        return -1;
    }

    open_files[pos].open = 1;

    if (pos >= 0xFF)
    {
        return -1;
    }

    int tmp = pos;

    ++pos;

    return tmp;
}

int close(int fd)
{
    if (open_files[fd].open != 1)
    {
        return -1;
    }

    open_files[fd].open = 0;

    return 0;
}

long int read(int fd, void *buffer, u32 count)
{
    if (open_files[fd].open != 1)
    {
        return -1;
    }

    char *b = buffer;

    u32 b_read = 0;
    s32 read_tmp;

    u32 current_sector =
        open_files[fd].sector + open_files[fd].cursor / SECTOR_SIZE;

    while (b_read < count && open_files[fd].cursor < open_files[fd].size)
    {
        u32 offset = open_files[fd].cursor % SECTOR_SIZE;

        u32 to_read = count - b_read;

        if ((read_tmp = atapi_read(b + b_read, current_sector, offset, to_read))
            == -1)
        {
            return -1;
        }

        b_read += read_tmp;
        open_files[fd].cursor += read_tmp;

        ++current_sector;
    }

    return b_read;
}

long seek(int fd, long offset, int whence)
{
    if (open_files[fd].open != 1)
    {
        return -1;
    }

    if (whence == SEEK_SET)
    {
        open_files[fd].cursor = offset;
    }
    else if (whence == SEEK_END)
    {
        open_files[fd].cursor = open_files[fd].size + offset;
    }
    else if (whence == SEEK_CUR)
    {
        open_files[fd].cursor += offset;
    }
    else
    {
        return -1;
    }

    return open_files[fd].cursor;
}