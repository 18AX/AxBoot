#include "isoreader.h"

#include "driver/atapi/atapi_driver.h"
#include "k/iso9660.h"
#include "utils/string.h"

static char *get_next(char *str, char delimiter)
{
    char *res = str;

    while (*res != '\0' && *res != delimiter)
    {
        res += 1;
    }

    return res;
}

static int file_name_eq(const char *str1, const char *str2)
{
    while (*str1 != '\0' && *str2 != '\0' && *str1 != ';' && *str2 != ';')
    {
        if (*str1 != *str2)
        {
            return 0;
        }
        ++str1;
        ++str2;
    }

    return (*str1 == '\0' && *str2 == ';') || (*str2 == '\0' && *str1 == ';');
}

/**
 * @param[in] entry path table directory entry
 * @param[in] filename the file name to find in the directory
 * @param[out] file the file structure to fill.
 * @return 1 if file found, -1 if not
 *
 * Try to find a file a a specified directory.
 */
static int lookup_dir(struct iso_path_table_le *entry, char *filename,
                      struct file *file)
{
    char buffer[SECTOR_SIZE];

    if (atapi_read_sector(buffer, entry->data_blk) == -1)
    {
        return -1;
    }

    u32 pos = 0;
    struct iso_dir *dir = (struct iso_dir*) buffer;

    while (pos < SECTOR_SIZE && dir->dir_size != 0)
    {
        if (file_name_eq(filename, dir->idf))
        {
            file->cursor = 0;
            file->sector = dir->data_blk.le;
            file->size = dir->file_size.le;
            return 1;
        }

        pos += dir->dir_size;
        dir = (struct iso_dir*) (buffer + pos);
    }

    return 0;
}

int iso_find_file(char *path, struct file *file)
{
    char buffer[SECTOR_SIZE];

    if (atapi_read_sector(buffer, 0x10) == -1)
    {
        return -1;
    }

    struct iso_prim_voldesc prim_voldesc;

    memcpy(&prim_voldesc, buffer, sizeof(struct iso_prim_voldesc));

    memset(buffer, 0, SECTOR_SIZE);

    if (atapi_read_sector(buffer, prim_voldesc.le_path_table_blk) == -1)
    {
        return -1;
    }

    u32 pos = 0;

    // We need to iterate over each child directories.
    char *begin = path;
    char *end = get_next(begin, '/');

    if (*end == '\0')
    {
        // root is the first entry

        struct iso_path_table_le *entry = (struct iso_path_table_le *)buffer;

        if (lookup_dir(entry, begin, file) == 1)
        {
            return 1;
        }
    }

    *end = '\0';

    int parent = -1;

    for (u32 i = 0; pos < prim_voldesc.path_table_size.le; ++i)
    {
        struct iso_path_table_le *entry = (struct iso_path_table_le*) (buffer + pos);

        // Check if the directory has the good parent
        if (parent == -1 || entry->parent_dir == parent)
        {
            char c = entry->idf[entry->idf_len];
            entry->idf[entry->idf_len] = '\0';
            if (strcmp(begin, entry->idf) == 0)
            {
                *end = '/';
                begin = end + 1;

                end = get_next(begin, '/');

                // That mean we are on the end of the path (so we are looking
                // for a file)
                if (*end == '\0')
                {
                    if (lookup_dir(entry, begin, file) == 1)
                    {
                        strcpy(file->path, path);
                        return 1;
                    }
                }
                else
                {
                    parent = i + 1;

                    *end = '\0';
                }
            }

            entry->idf[entry->idf_len] = c;
        }

        pos += sizeof(struct iso_path_table_le) + entry->idf_len;

        if (entry->idf_len % 2 != 0)
        {
            pos += 1;
        }
    }

    return -1;
}