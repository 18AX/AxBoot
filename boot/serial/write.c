__asm__(".code16gcc");


#if DEBUG
#include "write.h"

#include "io/io.h"
#include "serial.h"

int write(const char *buffer, u32 count)
{

    u32 i = 0;
    for (; i < count; ++i)
    {
        while ((inb(COM1 + 5) & (1 << 5)) == 0)
            ;
        outb(COM1, buffer[i]);
    }

    return i;
}

#endif
