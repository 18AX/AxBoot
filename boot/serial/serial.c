__asm__(".code16gcc");

#if (DEBUG)
#include "serial.h"

#include "io/io.h"
#include "k/types.h"

void init_serial(void)
{
    u8 saved_lcr = inb(COM1 + 3);

    outb(COM1 + 3, saved_lcr | 0x80);

    outb(COM1, 0x03);
    outb(COM1 + 1, 0x00);
    outb(COM1 + 3, 0x03);
    outb(COM1 + 3, 0x03);
    outb(COM1 + 2, 0xC7);

    outb(COM3 + 3, saved_lcr);
}

#endif