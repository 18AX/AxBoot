#ifndef WRITE_HEADER
#define WRITE_HEADER

#if DEBUG

#include "k/types.h"

int write(const char *buffer, u32 count);

#endif

#endif