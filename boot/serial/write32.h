#ifndef WRITE32_HEADER
#define WRITE32_HEADER

#if DEBUG

#include "k/types.h"

int write32(const char *buffer, u32 count);

#endif

#endif