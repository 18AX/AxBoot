#bin/sh

if [ $# != 3 ]
then
    echo "usage: ./iso.sh <iso to path> <output iso> <boot.cfg path>"
    exit 1
fi

echo "Patching $1"

mkdir /tmp/$1

mount -t iso9660 -o loop $1 /mnt/

tar cf - /mnt/ | (cd /tmp/$1; tar xfp -)

umount $1

cp boot/boot.bin /tmp/$1/mnt/boot.bin
cp kernel/kernel.bin /tmp/$1/mnt/boot/kernel.bin

cp $3 /tmp/$1/mnt/boot.cfg

xorriso -as mkisofs -o $2 -b boot.bin -c bootcat -no-emul-boot -boot-load-size 18 -boot-info-table -J -R -V "Custom iso" /tmp/$1/mnt

rm -r /tmp/$1