# AxBoot

32 bit Linux bootloader

## How to use

First, you need to compile the project.

```bash
make # If you want debug message on serial port build with make debug
```

Then, you have create a config file where you set up the linux you want to boot.

```bash
touch boot.cfg # Your config file
```

This is an example of a config file for Ubuntu.

```
kernel=casper/vmlinuz. init=casper/initrd. name=Ubuntu
```

Then you patch the iso you want

```bash
# First file is the input iso, the second one is the output
./iso.sh ubuntu-20.04.2.0-desktop-amd64.iso ubuntu-20.04.2.0-desktop-amd64.iso boot.cfg 
```

You can launch it in qemu.

```bash
qemu-system-x86_64 -serial stdio -cdrom boot.iso -m 4G 
```

## What's next ?

- Add a field in the config for the cmd line
- Improve the memory allocator
- Support more file system
