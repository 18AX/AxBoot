MODE=
.PHONY: all clean

all: boot.bin kernel.bin

debug: MODE=debug
debug: all

boot.bin:
	$(MAKE) -C boot $(MODE)

kernel.bin:
	$(MAKE) -C kernel $(MODE)

test:
	$(MAKE) -C tests

clean:
	$(MAKE) -C kernel clean
	$(MAKE) -C boot clean
	$(MAKE) -C tests clean
	$(RM) boot.img
	$(RM) iso_files/boot/boot.bin iso_files/boot/kernel.bin
	